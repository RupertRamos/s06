# Git-Merge Demonstration

The team will create a page that contains the list of the team members' name and their group name. They will search and read documentation about CSS Selectors and apply some design and layout to the title and list.


### Instructions:

1. Member 1 will create the files and boiler plate for html and link it to the css file.

2. Member 2 will a Header1 tag and write their group name. He will also create an unordered list and will write his name first.

3. Member 3 will also ad his name and the name of member one. He will also add an id attribute with a value "text-color" on the Header1 tag and use CSS Id selector to change its color to their preference.

4. Member 4 will also add his name and a class attribute on the Header1 tag with a value "text-center". Then he will use a CSS class selector to change the layout of the header element.

5. Member 5 will also add his name on the list and change the background  color of the body of the page. He will do this using the CSS tag selector.


### Solution


1. Member 1 will create the files and boiler plate for html and link it to the css file.

index.html

		<!DOCTYPE html>
		<html>
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>S06: Dev Skills and Dev Tools Intro</title>

			<link rel="stylesheet" type="text/css" href="./index.css">

		</head>
		<body>



		</body>
		</html>


2. Member 2 will a Header1 tag and write their group name. He will also create an unordered list and will write his name first.

index.html

		<body>

			<h1>Group Name</h1>


			<ul>
				<li>Member 2</li>
			</ul>

		</body>


3. Member 3 will also ad his name and the name of member one. He will also add an id attribute with a value "text-color" on the Header1 tag and use CSS Id selector to change its color to their preference.

index.html

		<body>

			<h1 id="text-color">Group Name</h1>


			<ul>
				<li>Member 1</li>
				<li>Member 2</li>
				<li>Member 3</li>
			</ul>

		</body>

index.css

		#text-color {
			color: darkred;
		}



4. Member 4 will also add his name and a class attribute on the Header1 tag and ul tag with a value "text-center". Then he will use a CSS class selector to change the layout of the header and ul elements to center the texts. He will also add a class with a value "list-style" for the li tags and create a CSS rule to remove the list style of the elements on the list.


index.html

		<body>

			<h1 id="text-color" class="text-center">Group Name</h1>


			<ul class="text-center">
				<li class="list-style">Member 1</li>
				<li class="list-style">Member 2</li>
				<li class="list-style">Member 3</li>
				<li class="list-style">Member 4</li>
			</ul>

		</body>

index.css

		.text-center {
			text-align: center;
		}

		.list-style {
			list-style: none;
		}


5. Member 5 will also add his name on the list and change the background  color of the body of the page and resize the font of the header and list elements to make it bigger (Search for rem unit). He will do this using the CSS tag selector.

index.html

		<body>

			<h1 id="text-color" class="text-center font-size-head">Group Name</h1>


			<ul class="text-center font-size-list">
				<li class="list-style">Member 1</li>
				<li class="list-style">Member 2</li>
				<li class="list-style">Member 3</li>
				<li class="list-style">Member 4</li>
				<li class="list-style">Member 4</li>
			</ul>

		</body>


index.css

		body {
			background-color: orange;
		}

		.font-size-head {
			font-size: 3rem;
		}

		.font-size-list {
			font-size: 2rem;
		}