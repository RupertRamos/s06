S06 Dev Skills and Dev Tools Introduction:

References:

Slide
https://docs.google.com/presentation/d/1Cy4y-53SZcxwkgxJmlrp40lLQgMS2TLqal74bNw03l0/edit#slide=id.g24d98d51652_0_3014

Batch Timeline:
https://docs.google.com/spreadsheets/d/11p7j3ZeqjMCm8eD0CLwu8mQ9FkOzTeq8R6UjYC-SEmI/edit#gid=0

Code Commit:
https://ap-southeast-1.console.aws.amazon.com/codesuite/codecommit/repositories/ZCBCurricula_MCP_WDC028v1.6.1/browse/refs/heads/master/--/frontend/s06/Manual.md?region=ap-southeast-1



======================================================================================
Best Practices for Documentation:

1. Use of effective search terms.

Search:

	version 1:
	How to make a button element unclickable?

	version 2:
	How to disable the button element?

	version 3:
	How to disable the button element in HTML?

	version 4:

	https://www.freecodecamp.org/news/how-to-google-like-a-pro-10-tips-for-effective-googling/

	How to disable the button element in HTML? site:w3schools.com
	



2. Read official documentation

	Sample Official Documentation for Some Technologies

	Bootstrap
	https://getbootstrap.com/docs/5.0/getting-started/introduction/

	ReactJS:
	https://legacy.reactjs.org/docs/getting-started.html

	JWT
	https://jwt.io/

	

3. Look for reliable sources of information

	Sample websites that are prefered by developers
	
	developer.mozilla.org
	https://developer.mozilla.org/en-US/docs/Web/javascript

	Javascript.info
	https://javascript.info/

	W3Schools
	https://www.w3schools.com/js/DEFAULT.asp

4. Scanning and reading

	- Refer to the slides


5. Test your understanding.

	- Refer to the slides



======================================================================================
Chat GPT demo

Since the most recent topic is about HTML Forms and Tables, let's demonstrate on how to create an HTML form using chatGPT

Initial Prompt
Please create an HTML form for registration that will require the following user inputs:
1. First Name
2. Last Name
3. Email
4. Mobile Number


Additional Prompts:

Can you please, add these input elements on the previous code:
1. Comments (using text area element)
2. Dropdown with options for choosing among 3 products: spoon, fork, and knife. Use select element for this input field.

======================================================================================

Create a ccs file for the layout and design. The form title  is "Registration Form" written in bold at the center-top of the page. Below the form title are the form inputs. The form inputs should be aligned center to the page as well. There should be proper margin on all sides each input field and make the width of the input field 80%. Apply apply 2 different font style for the form title and labels. Turn the label elements into block elements. Add color to to the form to make the form look lively.


======================================================================================
ChatGPT Result: https://chat.openai.com/?model=text-davinci-002-render-sha
======================================================================================

Note: The contextualize version of the code is found in the discussion folder of this repo:
https://gitlab.com/RupertRamos/s06/-/tree/master/discussion?ref_type=heads



======================================================================================
Boodle Bot Demo

1. In the General Text channel in discord, demonstrate the use of BoodleBot.
2. We will be asking the question: What are the different CSS Selectors?
3. Type command in chatbox to trigger the prompt:

	command:	/ask

4. Type the question after the prompt keyword.
5. BoodleBot will then generate a response.
6. Validate with students if everyone got the same response.
7. Try again. Ask them to form 2 questions and ask it to BoodleBot.